<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller_Login extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('model_login');

	}
	public function index(){
		$this->load->view('login.php');
	}
	public function login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$data['loggin'] = $this->model_login->login($username,$password);
		if ($data['loggin']) {
			redirect('/Controller_Home', 'refresh');
		}else{
			$data['hasil_login'] = 'Username/Password tidak valid';
			$this->load->view('login',$data);
		}
	}
	public function logout(){
		$this->session->unset_userdata('user_session_android');
		redirect('/Controller_Login', 'refresh');
	}

}