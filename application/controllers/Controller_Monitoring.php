	<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Controller_Monitoring extends CI_Controller {

		function __construct(){
			parent::__construct();
			$this->load->model('models');
		}

		public function index(){
			$data['data'] = $this->models->get_monitoring();
			$this->load->view('template/Sidebar');
			$this->load->view('monitoring.php',$data);
			$this->load->view('template/Footer');
		}

	}