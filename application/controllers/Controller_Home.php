	<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Controller_Home extends CI_Controller {
		function __construct(){
			parent::__construct();
			$this->load->library('session');
			$this->load->model('models');
			cek_session();

		} 
		public function index(){
			$data['data'] = $this->models->get_all();
			$this->load->view('template/Sidebar');
			$this->load->view('home.php',$data);
			$this->load->view('template/Footer');
		}

		public function edit(){
			$id_bhandle = $_GET['id_bhandle'];
			$data['payment'] = $this->models->get_all_payment();
			$data['data'] = $this->models->get_all_by_id($id_bhandle)[0];
			$this->load->view('template/Sidebar');
			$this->load->view('edit_bhandle',$data);
			$this->load->view('template/Footer');
		}
		public function input_bhandle(){
			$data['payment'] = $this->models->get_all_payment();
			$this->load->view('template/Sidebar');
			$this->load->view('bhandle.php',$data);
			$this->load->view('template/Footer');
		}


		public function save_bhandle(){
			$total = count($_FILES);
			
			$data = array(
				'id_aju'=> $_POST['aju']
			);
			for ($i=0; $i < $total; $i++) { 
				$file_exists = $_FILES[array_keys($_FILES)[$i]]['size'];
				if ($file_exists == 0) {
					continue;
				}

				$key = $_FILES[array_keys($_FILES)[$i]];
				$extension = pathinfo($key['name'], PATHINFO_EXTENSION);
				$actual_name = pathinfo($key['name'],PATHINFO_FILENAME);
				$file_name = $key['name'];
				$original_name = $actual_name;

				$j = 1;
				while(file_exists("./assets/uploads/bhandle/".$actual_name.".".$extension))
				{           
					$actual_name = $original_name.$j;
					$file_name = $actual_name.".".$extension;
					$j++;
				}

				$newFilePath = "./assets/uploads/bhandle/".$file_name;
				$tmpFilePath = $key['tmp_name'];

				if ($tmpFilePath != ""){
					if(move_uploaded_file($tmpFilePath, $newFilePath)) {
					}

				}
				$data[array_keys($_FILES)[$i]] = $file_name;
			}
			$this->models->save_bhandle($data);
			redirect('/Controller_Home', 'refresh');
		}
		
		public function save_edit_bhandle(){
			$total = count($_FILES);
			$id = $_POST['id_bhandle'];
			$data = array(
				'id_aju'=> $_POST['aju']
			);
			for ($i=0; $i < $total; $i++) { 
				$file_exists = $_FILES[array_keys($_FILES)[$i]]['size'];
				if ($file_exists == 0) {
					continue;
				}

				$key = $_FILES[array_keys($_FILES)[$i]];
				$extension = pathinfo($key['name'], PATHINFO_EXTENSION);
				$actual_name = pathinfo($key['name'],PATHINFO_FILENAME);
				$file_name = $key['name'];
				$original_name = $actual_name;

				$j = 1;
				while(file_exists("./assets/uploads/bhandle/".$actual_name.".".$extension))
				{           
					$actual_name = $original_name.$j;
					$file_name = $actual_name.".".$extension;
					$j++;
				}

				$newFilePath = "./assets/uploads/bhandle/".$file_name;
				$tmpFilePath = $key['tmp_name'];

				if ($tmpFilePath != ""){
					if(move_uploaded_file($tmpFilePath, $newFilePath)) {
					}

				}
				$data[array_keys($_FILES)[$i]] = $file_name;
			}
			$this->models->save_edit_bhandle($id,$data);
			redirect('/Controller_Home', 'refresh');


		}
		public function delete($id){
			$this->models->delete($id);
			redirect('/Controller_Home', 'refresh');
		}




	}