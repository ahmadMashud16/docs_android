
<!--
    Author: W3layouts
    Author URL: http://w3layouts.com
    License: Creative Commons Attribution 3.0 Unported
    License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>CSJ system</title>
    <!-- Meta tag Keywords -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Green Life web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design"
    />
    <script>
        addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!--// Meta tag Keywords -->
    <!-- css files -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
    <!-- Bootstrap-css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" type="text/css" media="all" />
    <!-- Style-css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.css">
    <!-- Font-Awesome-Icons-css -->
    <link href="<?php echo base_url(); ?>assets/css/popuo-box.css" rel="stylesheet" type="text/css" media="all" />
    <!-- Popup css (for Video Popup) -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/lightbox.css" type="text/css" media="all">
    <!-- Lightbox css (for Projects) -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/flexslider.css" type="text/css" media="screen" property="" />
    <!-- Flexslider css (for Testimonials) -->
    <!-- //css files -->
    <!-- web-fonts -->

    <!-- //web-fonts -->
</head>

<body>
    <div class="main-agile">
        <!-- banner -->
        <div class="agile-top">
            <div class="col-xs-4 logo">

            </div>
            <div class="col-xs-6 header-w3l">

            </div>

        </div>
    </div>
    <!-- signin Model -->


    <br>
    <br>
    <br>
    <br>
    <br>
    <br>

    <center>
        <form  method="POST" action="<?php echo base_url();?>Controller_Login/login">
            <img src="<?php echo base_url(); ?>assets/images/csjj.png"> <br>
            <input type="text" name="username" placeholder="username"><br>
            <br>
            <input type="password" name="password" placeholder="password"><hr>
            <button  type="submit" class="btn btn-success btn-lg btn-radius" >Login</button>
        </form>
    </center>




    <!-- jquery -->
    <script src="<?php echo base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>
    <!-- //jquery -->

    <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
    <!-- Necessary-JavaScript-File-For-Bootstrap -->

    <!--  light box js -->
    <script src="<?php echo base_url(); ?>assets/js/lightbox-plus-jquery.min.js"></script>
    <!-- //light box js-->
    
    <!-- stats numscroller-js-file -->
    <script src="<?php echo base_url(); ?>assets/js/numscroller-1.0.js"></script>
    <!-- //stats numscroller-js-file -->
    
    <!-- Baneer-js -->
    <script src="<?php echo base_url(); ?>assets/js/responsiveslides.min.js"></script>
    <script>
        // You can also use "$(window).load(function() {"
        $(function () {
            // Slideshow 4
            $("#slider4").responsiveSlides({
                auto: true,
                pager: true,
                nav: false,
                speed: 500,
                namespace: "callbacks",
                before: function () {
                    $('.events').append("<li>before event fired.</li>");
                },
                after: function () {
                    $('.events').append("<li>after event fired.</li>");
                }
            });

        });
    </script>
    <!-- //Baneer-js -->

    <!-- navigation -->
    <script>
        (function ($) {
            // Menu Functions
            $(document).ready(function () {
                $('#menuToggle').click(function (e) {
                    var $parent = $(this).parent('.menu');
                    $parent.toggleClass("open");
                    var navState = $parent.hasClass('open') ? "hide" : "show";
                    $(this).attr("title", navState + " navigation");
                    // Set the timeout to the animation length in the CSS.
                    setTimeout(function () {
                        console.log("timeout set");
                        $('#menuToggle > span').toggleClass("navClosed").toggleClass("navOpen");
                    }, 200);
                    e.preventDefault();
                });
            });
        })(jQuery);
    </script>
    <!-- //navigation -->

    <!-- pop-up(for video popup)-->
    <script src="<?php echo base_url(); ?>assets/js/jquery.magnific-popup.js"></script>
    <script>
        $(document).ready(function () {
            $('.popup-with-zoom-anim').magnificPopup({
                type: 'inline',
                fixedContentPos: false,
                fixedBgPos: true,
                overflowY: 'auto',
                closeBtnInside: true,
                preloader: false,
                midClick: true,
                removalDelay: 300,
                mainClass: 'my-mfp-zoom-in'
            });

        });
    </script>
    <!-- //pop-up-box (syllabus section video)-->
    
    <!-- video js (background) -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.vide.min.js"></script>
    <!-- //video js (background) -->

    <!-- smoothscroll -->
    <script src="<?php echo base_url(); ?>assets/js/SmoothScroll.min.js"></script>
    <!-- //smoothscroll -->

    <!-- password-script -->
    <script>
        window.onload = function () {
            document.getElementById("password1").onchange = validatePassword;
            document.getElementById("password2").onchange = validatePassword;
        }

        function validatePassword() {
            var pass2 = document.getElementById("password2").value;
            var pass1 = document.getElementById("password1").value;
            if (pass1 != pass2)
                document.getElementById("password2").setCustomValidity("Passwords Don't Match");
            else
                document.getElementById("password2").setCustomValidity('');
            //empty string means no validation error
        }
    </script>
    <!-- //password-script -->

    <!-- start-smooth-scrolling -->
    <script src="<?php echo base_url(); ?>assets/js/move-top.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/easing.js"></script>
    <script>
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();

                $('html,body').animate({
                    scrollTop: $(this.hash).offset().top
                }, 1000);
            });
        });
    </script>
    <!-- //end-smooth-scrolling -->

    <!-- smooth scrolling-bottom-to-top -->
    <script>
        $(document).ready(function () {
            /*
                var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear' 
                };
                */
                $().UItoTop({
                    easingType: 'easeOutQuart'
                });
            });
        </script>
        <a href="#" id="toTop" style="display: block;">
            <span id="toTopHover" style="opacity: 1;"> </span>
        </a>
        <!-- //smooth scrolling-bottom-to-top -->

        <!-- flexSlider (for testimonials) -->
        <script defer src="<?php echo base_url(); ?>assets/js/jquery.flexslider.js"></script>
        <script>
            $(window).load(function () {
                $('.flexslider').flexslider({
                    animation: "slide",
                    start: function (slider) {
                        $('body').removeClass('loading');
                    }
                });
            });
        </script>
        <!-- //flexSlider (for testimonials) -->

        <!-- //js-scripts -->

    </body>

    </html>