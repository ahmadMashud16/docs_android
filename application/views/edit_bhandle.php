<style type="text/css">
	hr{
		border: 1px solid black;
	}
	div.gallery {
		margin: 5px;
		border: 1px solid #ccc;
		float: left;
		width: 180px;
	}

	div.gallery:hover {
		border: 1px solid #777;
	}

	div.gallery img {
		width: 100%;
		height: auto;
	}

	div.desc {
		padding: 15px;
		text-align: center;
		white-space: nowrap;
		overflow: hidden;
		text-overflow: ellipsis;
	}
</style>
<div style="padding: 50px">
	<div>
		<form method="post" action="<?php echo base_url(); ?>Controller_Home/save_edit_bhandle" enctype="multipart/form-data">
			<h3>Edit Bahandle</h3>
			<br>
			<h4 style="background-color: #b7b7b7;">ID AJU</h4>
			<br>
			<select class="" id="aju"  name="aju"> 
				<option value=""> -- Pilih No AJU -- </option>
				<?php  foreach ($payment as $key) { ?>
					<option <?php echo $data->id_aju == $key->no_aju_custom ? 'selected' : ''; ?> value="<?php echo $key->no_aju_custom; ?>"><?php echo $key->no_aju_custom; ?></option>
				<?php } ?>
			</select>
			<hr>
			<h4 style="background-color:#b7b7b7;">Seal Awal</h4>
			<input type="hidden" name="id_bhandle" value="<?php echo $data->id_bhandle ?>">
			<br>

			<div class="gallery">
				<a target="_blank" href="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->first_seal; ?>">
					<img src="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->first_seal; ?>"  width="400" height="200">
				</a>
				<div class="desc"><?php echo $data->first_seal; ?></div>
			</div>
			<input type="file" name="first_seal" accept="image/*"  >
			<br>

			<h4  style="background-color:#b7b7b7;">Posisi Awal Barang</h4>
			<br>


			<div class="gallery">
				<a target="_blank" href="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->first_position_good1; ?>">
					<img src="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->first_position_good1; ?>"  width="400" height="200">
				</a>
				<div class="desc"><?php echo $data->first_position_good1; ?></div>
			</div>
			<input type="file" name="first_position_good1" accept="image/*"  >
			<br>
			<hr >


			<div class="gallery">
				<a target="_blank" href="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->first_position_good2; ?>">
					<img src="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->first_position_good2; ?>"  width="400" height="200">
				</a>
				<div class="desc"><?php echo $data->first_position_good2 == null ? "file belum diupload" : $data->first_position_good2; ?></div>
			</div>
			<input type="file" name="first_position_good2" accept="image/*"  >
			<hr>

			<div class="gallery">
				<a target="_blank" href="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->first_position_good3; ?>">
					<img src="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->first_position_good3; ?>"  width="400" height="200">
				</a>
				<div class="desc"><?php echo $data->first_position_good3 == null ? "file belum diupload" : $data->first_position_good3; ?></div>
			</div>
			<input type="file" name="first_position_good3" accept="image/*"  >
			<br>

			<h4 style="background-color: #b7b7b7;">Peroses Bhandle</h4>
			<br>


			<div class="gallery">
				<a target="_blank" href="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->bhandle_proses1; ?>">
					<img src="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->bhandle_proses1; ?>"  width="400" height="200">
				</a>
				<div class="desc"><?php echo $data->bhandle_proses1 == null ? "file belum diupload" : $data->bhandle_proses1; ?></div>
			</div>
			<input type="file" name="bhandle_proses1" accept="image/*"  >
			<hr>

			<div class="gallery">
				<a target="_blank" href="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->bhandle_proses2; ?>">
					<img src="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->bhandle_proses2; ?>"  width="400" height="200">
				</a>
				<div class="desc"><?php echo $data->bhandle_proses2 == null ? "file belum diupload" : $data->bhandle_proses2; ?></div>
			</div>
			<input type="file" name="bhandle_proses2" accept="image/*"  >
			<hr>


			<div class="gallery">
				<a target="_blank" href="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->bhandle_proses3; ?>">
					<img src="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->bhandle_proses3; ?>"  width="400" height="200">
				</a>
				<div class="desc"><?php echo $data->bhandle_proses3 == null ? "file belum diupload" : $data->bhandle_proses3; ?></div>
			</div>
			<input type="file" name="bhandle_proses3" accept="image/*"  >
			<hr>

			
			<div class="gallery">
				<a target="_blank" href="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->bhandle_proses4; ?>">
					<img src="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->bhandle_proses4; ?>"  width="400" height="200">
				</a>
				<div class="desc"><?php echo $data->bhandle_proses4 == null ? "file belum diupload" : $data->bhandle_proses4; ?></div>
			</div>
			<input type="file" name="bhandle_proses4" accept="image/*"  >
			<hr>

			
			<div class="gallery">
				<a target="_blank" href="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->bhandle_proses5; ?>">
					<img src="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->bhandle_proses5; ?>"  width="400" height="200">
				</a>
				<div class="desc"><?php echo $data->bhandle_proses5 == null ? "file belum diupload" : $data->bhandle_proses5; ?></div>
			</div>
			<input type="file" name="bhandle_proses5" accept="image/*"  >
			<hr>

			
			<div class="gallery">
				<a target="_blank" href="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->bhandle_proses6; ?>">
					<img src="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->bhandle_proses6; ?>"  width="400" height="200">
				</a>
				<div class="desc"><?php echo $data->bhandle_proses6 == null ? "file belum diupload" : $data->bhandle_proses6; ?></div>
			</div>
			<input type="file" name="bhandle_proses6" accept="image/*"  >
			<hr>

			
			<div class="gallery">
				<a target="_blank" href="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->bhandle_proses7; ?>">
					<img src="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->bhandle_proses7; ?>"  width="400" height="200">
				</a>
				<div class="desc"><?php echo $data->bhandle_proses7 == null ? "file belum diupload" : $data->bhandle_proses7; ?></div>
			</div>
			<input type="file" name="bhandle_proses7" accept="image/*"  >
			<hr>

			
			<div class="gallery">
				<a target="_blank" href="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->bhandle_proses8; ?>">
					<img src="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->bhandle_proses8; ?>"  width="400" height="200">
				</a>
				<div class="desc"><?php echo $data->bhandle_proses8 == null ? "file belum diupload" : $data->bhandle_proses8; ?></div>
			</div>
			<input type="file" name="bhandle_proses8" accept="image/*"  >
			<hr>

			
			<div class="gallery">
				<a target="_blank" href="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->bhandle_proses9; ?>">
					<img src="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->bhandle_proses9; ?>"  width="400" height="200">
				</a>
				<div class="desc"><?php echo $data->bhandle_proses9 == null ? "file belum diupload" : $data->bhandle_proses9; ?></div>
			</div>
			<input type="file" name="bhandle_proses9" accept="image/*"  >
			<hr>

			
			<div class="gallery">
				<a target="_blank" href="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->bhandle_proses10; ?>">
					<img src="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->bhandle_proses10; ?>"  width="400" height="200">
				</a>
				<div class="desc"><?php echo $data->bhandle_proses10 == null ? "file belum diupload" : $data->bhandle_proses10; ?></div>
			</div>
			<input type="file" name="bhandle_proses10" accept="image/*"  >
			<hr>

			
			<div class="gallery">
				<a target="_blank" href="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->bhandle_proses11; ?>">
					<img src="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->bhandle_proses11; ?>"  width="400" height="200">
				</a>
				<div class="desc"><?php echo $data->bhandle_proses11 == null ? "file belum diupload" : $data->bhandle_proses11; ?></div>
			</div>
			<input type="file" name="bhandle_proses11" accept="image/*"  >
			<hr>

			
			<div class="gallery">
				<a target="_blank" href="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->bhandle_proses12; ?>">
					<img src="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->bhandle_proses12; ?>"  width="400" height="200">
				</a>
				<div class="desc"><?php echo $data->bhandle_proses12 == null ? "file belum diupload" : $data->bhandle_proses12; ?></div>
			</div>
			<input type="file" name="bhandle_proses12" accept="image/*"  >
			<br>

			<h4 style="background-color: #b7b7b7;">Selesai Packing Dicostumer</h4>
			<br>

			
			<div class="gallery">
				<a target="_blank" href="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->done_packing1; ?>">
					<img src="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->done_packing1; ?>"  width="400" height="200">
				</a>
				<div class="desc"><?php echo $data->done_packing1 == null ? "file belum diupload" : $data->done_packing1; ?></div>
			</div>
			<input type="file" name="done_packing1" accept="image/*"  >
			<hr>

			
			<div class="gallery">
				<a target="_blank" href="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->done_packing2; ?>">
					<img src="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->done_packing2; ?>"  width="400" height="200">
				</a>
				<div class="desc"><?php echo $data->done_packing2 == null ? "file belum diupload" : $data->done_packing2; ?></div>
			</div>
			<input type="file" name="done_packing2" accept="image/*"  >
			<hr>

			
			<div class="gallery">
				<a target="_blank" href="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->done_packing3; ?>">
					<img src="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->done_packing3; ?>"  width="400" height="200">
				</a>
				<div class="desc"><?php echo $data->done_packing3 == null ? "file belum diupload" : $data->done_packing3; ?></div>
			</div>
			<input type="file" name="done_packing3" accept="image/*"  >
			<br>

			<h4 style="background-color: #b7b7b7;">Seal Akhir</h4>
			<br>

			
			<div class="gallery">
				<a target="_blank" href="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->last_seal; ?>">
					<img src="<?php echo base_url() ?>assets/uploads/bhandle/<?php echo $data->last_seal; ?>"  width="400" height="200">
				</a>
				<div class="desc"><?php echo $data->last_seal == null ? "file belum diupload" : $data->last_seal; ?></div>
			</div>
			<input type="file" name="last_seal" accept="image/*"  >
			<hr>

			<button type="submit" class="btn btn-success btn-lg btn-radius">UPDATE</button>
		</form>
	</center>
	<br>

	<br>

	<br>

	<br>
	<br><br>

	<br>

	<br>

	<br>
	<br>
</div>
</div>
