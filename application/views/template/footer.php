

<!-- jquery -->
<script src="<?php echo base_url(); ?>assets/js/jquery-2.1.4.min.js"></script>
<!-- //jquery -->

<script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
<!-- Necessary-JavaScript-File-For-Bootstrap -->

<!--  light box js -->
<script src="<?php echo base_url(); ?>assets/js/lightbox-plus-jquery.min.js"></script>
<!-- //light box js-->

<!-- stats numscroller-js-file -->
<script src="<?php echo base_url(); ?>assets/js/numscroller-1.0.js"></script>
<!-- //stats numscroller-js-file -->

<!-- Baneer-js -->
<script src="<?php echo base_url(); ?>assets/js/responsiveslides.min.js"></script>


<script src="<?php echo base_url() ?>assets/vendors/jqvmap/dist/jquery.vmap.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
<script src="<?php echo base_url() ?>assets/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>

<!--table-->
<script src="<?php echo base_url() ?>assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendors/jszip/dist/jszip.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendors/pdfmake/build/pdfmake.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendors/pdfmake/build/vfs_fonts.js"></script>
<script src="<?php echo base_url() ?>assets/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url() ?>assets/vendors/datatables.net-buttons/js/buttons.colVis.min.js"></script>
<script src="<?php echo base_url() ?>assets/assets/js/init-scripts/data-table/datatables-init.js"></script>

<script src="<?php echo base_url() ?>assets/js/common.js"></script>
<script src="<?php echo base_url() ?>assets/js/bhandle.js"></script>
<script>
        // You can also use "$(window).load(function() {"
        $(function () {
            // Slideshow 4
            $("#slider4").responsiveSlides({
                auto: true,
                pager: true,
                nav: false,
                speed: 500,
                namespace: "callbacks",
                before: function () {
                    $('.events').append("<li>before event fired.</li>");
                },
                after: function () {
                    $('.events').append("<li>after event fired.</li>");
                }
            });

        });
    </script>
    <!-- //Baneer-js -->

    <!-- navigation -->
    <script>
        (function ($) {
            // Menu Functions
            $(document).ready(function () {
                $('#menuToggle').click(function (e) {
                    var $parent = $(this).parent('.menu');
                    $parent.toggleClass("open");
                    var navState = $parent.hasClass('open') ? "hide" : "show";
                    $(this).attr("title", navState + " navigation");
                    // Set the timeout to the animation length in the CSS.
                    setTimeout(function () {
                        console.log("timeout set");
                        $('#menuToggle > span').toggleClass("navClosed").toggleClass("navOpen");
                    }, 200);
                    e.preventDefault();
                });
            });
        })(jQuery);
    </script>
    <!-- //navigation -->

    <!-- pop-up(for video popup)-->
    <script src="<?php echo base_url(); ?>assets/js/jquery.magnific-popup.js"></script>
    <script>
        $(document).ready(function () {
            $('.popup-with-zoom-anim').magnificPopup({
                type: 'inline',
                fixedContentPos: false,
                fixedBgPos: true,
                overflowY: 'auto',
                closeBtnInside: true,
                preloader: false,
                midClick: true,
                removalDelay: 300,
                mainClass: 'my-mfp-zoom-in'
            });

        });
    </script>
    <!-- //pop-up-box (syllabus section video)-->
    
    <!-- video js (background) -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.vide.min.js"></script>
    <!-- //video js (background) -->

    <!-- smoothscroll -->
    <script src="<?php echo base_url(); ?>assets/js/SmoothScroll.min.js"></script>
    <!-- //smoothscroll -->

    <!-- password-script -->
    <!-- //password-script -->

    <!-- start-smooth-scrolling -->
    <script src="<?php echo base_url(); ?>assets/js/move-top.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/easing.js"></script>
    <script>
        jQuery(document).ready(function ($) {
            $(".scroll").click(function (event) {
                event.preventDefault();

                $('html,body').animate({
                    scrollTop: $(this.hash).offset().top
                }, 1000);
            });
        });
    </script>
    <!-- //end-smooth-scrolling -->

    <!-- smooth scrolling-bottom-to-top -->
    <script>
        $(document).ready(function () {
            /*
                var defaults = {
                containerID: 'toTop', // fading element id
                containerHoverID: 'toTopHover', // fading element hover id
                scrollSpeed: 1200,
                easingType: 'linear' 
                };
                */
                $().UItoTop({
                    easingType: 'easeOutQuart'
                });
            });
        </script>
        <a href="#" id="toTop" style="display: block;">
            <span id="toTopHover" style="opacity: 1;"> </span>
        </a>
        <!-- //smooth scrolling-bottom-to-top -->

        <!-- flexSlider (for testimonials) -->
        <script defer src="<?php echo base_url(); ?>assets/js/jquery.flexslider.js"></script>
        <script>
            $(window).load(function () {
                $('.flexslider').flexslider({
                    animation: "slide",
                    start: function (slider) {
                        $('body').removeClass('loading');
                    }
                });
            });
        </script>
        <!-- //flexSlider (for testimonials) -->

        <!-- //js-scripts -->

    </body>
  </html>