 <center>
  <style>
    table, td, th {
      border: 1px solid black;
      text-align: center;
    }
  </style>

  <br>
  <!-- <h2>LIST BAHANDLE</h2> -->
  <div style="padding: 15px">

    <div class="row">

      <div class="col-md-12">
        <div class="card">
          <div class="card-header">
          </div>
          <div class="card-body">
            <table  id="dataTable" style="width:60% border:1px;text-align: center;">
              <thead >
                <th >No Folder</th>
                <th >Description</th>
                <th >Shipper</th>
                <th>Quotation No</th>
                <th>Customer Name</th>
                <!-- <th>PPJK</th> -->
                <th>QTY</th>
                <th>ETD</th>
                <th>ETA</th>
                <th>ORI DOC</th>
                <th>AJU</th>
                <th>DRAFT</th>
                <th>E BILL</th>
                <th>CT PIB</th>
                <th>AMOUNT</th>
                <th>SPJM</th>
                <th>SPJK</th>
                <th>SPPB</th>
                <th>CT SPPB</th>
                <th>DEL DATE</th>
                <th>Remarks</th>
              </thead>
              <tbody >
               <?php 
               $months = ['1' => 'Januari', '2'=>'Februari', '3'=>'Maret', '4'=>'April',
               '5' => 'Mei', '6'=>'Juni', '7'=>'Juli', '8'=>'Agustus','9' => 'Sepetember', '10'=>'Oktober', '11'=>'November', '12'=>'Desember'];
               foreach ($data as $key) {
                 ?>
                 <form  autocomplete="off" action="<?php echo base_url() ?>Controller_Home/save_edit_monitoring" method="POST">
                   <tr>
                    <input type="hidden" name="id_quatation" value="<?php echo $key->id_quatation;  ?>">
                    <td><?php echo $key->no_folder;  ?></td>
                    <td><?php echo $key->description ?></td>
                    <td><?php echo $key->shipper ?></td>
                    <td><?php echo $key->quatation_no;  ?></td>
                    <td><?php echo $key->customer;  ?></td>
                    <!--        <td><input type="text" name="ppjk"  value="<?php echo $key->ppjk;  ?>"></td> -->
                    <td><?php echo $key->qty;  ?></td>
                    <td><?php echo $key->etd != null ? date("d M Y", strtotime($key->etd)) : "-";?></td> 
                    <td><?php echo $key->eta != null ? date("d M Y", strtotime($key->eta)) : "-";?></td>
                    <td><?php echo $key->ori_doc;  ?></td>
                    <td><?php echo $key->no_aju;  ?></td>
                    <td><?php echo $key->draft;  ?></td>
                    <td><?php echo $key->bill_date != null ? date("d M Y", strtotime($key->bill_date)) : "-";?></td>
                    <?php 
                    $key->ct_pib  = $key->ct_pib < 0 ? ($key->ct_pib*-1) : $key->ct_pib ;
                    $ct_pib_background = 'white';
                    if ( $key->ct_pib <= 3) {
                      $ct_pib_background = 'grey';
                    }else if ( $key->ct_pib > 3 && $key->ct_pib < 6) {
                      $ct_pib_background = 'yellow';
                    } else if ( $key->ct_pib >= 6) {
                      $ct_pib_background = 'red';
                    }
                    ?>
                    <td style="background-color: <?php echo $ct_pib_background; ?>"><?php echo $key->ct_pib;  ?></td>         

                    <td>Rp. <?php echo  number_format($key->bpn,2,",","."); ?></td>
                    <td style="<?php echo $key->spjm != null ? 'background-color: red' : null ?>"> <?php echo $key->spjm != null ? date("d-m-Y", strtotime($key->spjm)) : "-";?></td>
                    <td style="<?php echo $key->spjk != null ? 'background-color: yellow' : null ?>"><?php echo $key->spjk != null ? date("d-m-Y", strtotime($key->spjk)) : "-";?></td>
                    <td style="<?php echo $key->sppb != null ? 'background-color: green' : null ?>"><?php echo $key->sppb != null ? date("d-m-Y", strtotime($key->sppb)) : "-";?></td>
                    <?php 
                    $key->ct_sppb  = $key->ct_sppb < 0 ? ($key->ct_sppb*-1) : $key->ct_sppb ;
                    $ct_sppb_background = 'white';
                    if ( $key->ct_sppb <= 3) {
                      $ct_sppb_background = 'grey';
                    }else if ( $key->ct_sppb > 3 && $key->ct_sppb < 6 ) {
                      $ct_sppb_background = 'yellow';
                    } else if ( $key->ct_sppb >= 6) {
                      $ct_sppb_background = 'red';
                    }
                    ?>
                    <td style="background-color: <?php echo $ct_sppb_background; ?>"><?php echo $key->ct_sppb;  ?></td>
                    <td><?php echo $key->delivery_date != null ? date("d M Y", strtotime($key->delivery_date)) : "-";?></td>
                    <td><?php echo $key->remarks;  ?></td>
                  </tr>
                </form>
                <?php 
              }

              ?> 
            </tbody>
          </table>
        </div>
      </div>
    </div>


  </div>
</div>

</center>

