<?php
class models extends ci_model{

	public function __construct() 
	{
		parent::__construct(); 
		$this->load->database();
	}

	public function get_all_payment(){
		$query= "SELECT *,right(no_aju,6) as no_aju_custom  FROM `tbl_payment`";
		return $this->db->query($query)->result();
	}

	public function get_all(){
		$query= "SELECT * FROM tbl_bhandle";
		return $this->db->query($query)->result();
	}
	public function get_all_by_id($id_bhandle){
		$query = "SELECT * FROM tbl_bhandle where id_bhandle =".$id_bhandle;
		$data = $this->db->query($query)->result();
		return $data;
	}

	function save_bhandle($data)
	{
		$status_save = false;
		$this->db->insert('tbl_bhandle', $data);
		return true;
	}
	function save_edit_bhandle($id,$data)
	{
		$status_save = false;
		$this->db->where('id_bhandle', $id);
		$this->db->update('tbl_bhandle', $data);
		return true;
	}
	function delete($id)
	{
		$status_save =false;
		$this->db->where('id_bhandle', $id);
		$this->db->delete('tbl_bhandle');
		return true;
	}

	public function get_monitoring(){
		$query= "select tbi.shipper,tq.no_folder,tq.ppjk,tq.draft,tq.ori_doc,tq.remarks,tq.id_quatation,tcc.pt_name as consignee,
		tq.item_name as description,
		tc.id_customer  as id_customer,
		tc.name  as customer,
		CONCAT(tq.qty , 'x',tms.size_type)  as qty,
		tq.quatate  as quatation_no,
		tfb.date_etd as etd,
		tfb.date_eta as eta,
		tp.no_aju ,
		tb.date as bill_date ,
		case when tb.date is not null then 
		datediff(tfb.date_eta,tb.date)
		else 
		datediff(tfb.date_eta,now()) end as ct_pib,
		case when tpb.sppb is not null then
		datediff(tb.date,tpb.sppb)
		else 
		datediff(tb.date,now()) end as ct_sppb,
		0 as amount,
		td.unloading_date as delivery_date,
		td.address as address,
		tpb.spjk,
		tpb.spjm,
		tpb.sppb,
		tb.bpn,
		tq.invoice_date,
		tq.paid_date
		from
		tbl_quatation tq
		left join tbl_m_consignee tcc on
		tcc.id_m_consignee = tq.id_consignee
		left join tbl_m_customer tc on
		tq.id_customer = tc.id_m_customer
		left join tbl_billing tb on
		tb.id_quatation = tq.id_quatation
		left join tbl_portal_bc tpb on
		tpb.id_quatation = tq.id_quatation
		left join tbl_form_bl tfb on
		tfb.id_quatation = tq.id_quatation
		left join tbl_m_size_container tms on
		tq.id_size_container = tms.id_size_container
		left join tbl_payment tp on
		tp.id_quatation = tq.id_quatation
		left join tbl_delivery td on
		td.id_quatation = tq.id_quatation
		left join 
		(select shipper, id_quatation from tbl_invoice_all_in 
		 union 
		 select shipper, id_quatation from tbl_invoice_apb
		 union 
		 select shipper, id_quatation from tbl_invoice_ddu ) 
		tbi on 
		tbi.id_quatation = tq.id_quatation
		where (tpb.sppb is null or (tpb.sppb  >= DATE_SUB(CURDATE(), INTERVAL 5 DAY) AND tpb.sppb  <= CURDATE())) ";
		return $this->db->query($query)->result();
	}
}